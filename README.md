
docker-run-me-once
==================
@author  : pete.henshall@phc-digital.com 20220207

@licence : GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 (see LICENCE.md)

@version : v0.0.3.20220207


Intro
=====
A bash script to connect to a docker container and run a command once and only once.  Made to stop multiple 
processes piling up and killing the server or deal with flock or dblock implementations which can't handle 
out of the blue reboots or crashed processes ie. those processes that do something unexpected.  This will 
actually check if the process is running correctly inside the container.

run ./docker-run-me-once-in-a-container for the usage guide


Still to do
===========
@todo - Check the log dir exists before writing
@todo - If a bad error occurs alert someone/route don't just log?
@todo - Check docker is installed
@todo - Check the container name is correct/exists else fail
@todo - Check there aren't any zombie processes hanging around?  Other things like this, edge cases?


